package CSS::Validation::Comfy;

use 5.010;
use strict;
use warnings;

use Data::Printer;
use File::ChangeNotify;
use LWP::UserAgent;
use Moose;
use WebService::Validator::CSS::W3C;

use feature qw(say);

=head1 NAME

CSS::Validation::Comfy - The great new CSS::Validation::Comfy!

=head1 VERSION

Version 0.01

=cut

our $VERSION = '0.002';


my @watched_directories = (
    '/home/smonff/etyssa/src/gityssa/webcontent/style/'
);

# local validator URI
my $localhost = 'http://v.css:8080/css-validator/';

sub instantiate_watcher {
    my $self = shift;
    my $watcher = File::ChangeNotify->instantiate_watcher(
        # Must go in configuration
        directories => @watched_directories,
        filter      => qr/\.(?:css|scss)$/,
        );
    return $watcher;
}

sub instantiate_css_validator {
    my $self = shift;
    my $validator = WebService::Validator::CSS::W3C->new;
    my $uri = $validator->validator_uri($localhost);
    my $status = $self->is_validator_available($localhost);

    if ($status->is_success) {
        say "Validator available";
        return $validator;
    } else {
        die "Validator not available";
    }
}

sub is_validator_available {
    my ($self, $validator_uri) = @_;
    my $ua = LWP::UserAgent->new;
    my $response = $ua->get($validator_uri);
    return $response;
}

=Head1 SYNOPSIS

Quick summary of what the module does.

Perhaps a little code snippet.

    use CSS::Validation::Comfy;

    my $foo = CSS::Validation::Comfy->new();
    ...

=head1 EXPORT

A list of functions that can be exported.  You can delete this section
if you don't export anything, such as for a purely object-oriented module.

=head1 SUBROUTINES/METHODS

=head2 instantiate_watcher



=cut

=head2 function2

=cut

=head1 AUTHOR

Sébastien Feugère, C<< <smonff at riseup.net> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-css-validation-comfy at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=CSS-Validation-Comfy>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.




=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc CSS::Validation::Comfy


You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=CSS-Validation-Comfy>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/CSS-Validation-Comfy>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/CSS-Validation-Comfy>

=item * Search CPAN

L<http://search.cpan.org/dist/CSS-Validation-Comfy/>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

Copyright 2016 Sébastien Feugère.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


=cut

1; # End of CSS::Validation::Comfy
