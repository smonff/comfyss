NAME

    CSS-Validation-Comfy (comfyss)

SYNOPSIS

    Comfortable Style Sheets conformance and validation.

DESCRIPTION

    The idea is to focus on a permanent validation and not having to
    trigger tests by hand: each time you change your CSS, you must be
    sure it validates.

    The README is used to introduce the module and provide
    instructions on how to install the module, any machine
    dependencies it may have (for example C compilers and installed
    libraries) and any other information that should be provided
    before the module is installed.

    A README file is required for CPAN modules since CPAN extracts the
    README file from a module distribution so that people browsing the
    archive can use it to get an idea of the module's uses. It is
    usually a good idea to provide version information here so that
    people can decide whether fixes for the module are worth
    downloading.

INSTALLATION

    The simple way:

        cpanm CSS::Validation::Comfy

    Or run the following commands:

        perl Makefile.PL make make test make install

SUPPORT AND DOCUMENTATION

    After installing, you can find documentation for this module with
    the perldoc command.

        perldoc CSS::Validation::Comfy

    The module can be forked from Gitlab. It is not hosted on Github
    but feel free to deport it there.

        git clone git@git.framasoft.org:smonff/comfyss.git

    You can also look for information at:

        * Issues and features tracker on a Gitlab instance
            https://git.framasoft.org/smonff/comfyss/issues

        * RT, CPAN's request tracker (you can also report bugs here)
            http://rt.cpan.org/NoAuth/Bugs.html?Dist=CSS-Validation-Comfy

        * AnnoCPAN, Annotated CPAN documentation
            http://annocpan.org/dist/CSS-Validation-Comfy

        * CPAN Ratings
            http://cpanratings.perl.org/d/CSS-Validation-Comfy

        * Search CPAN
            http://search.cpan.org/dist/CSS-Validation-Comfy/


LICENSE AND COPYRIGHT

    Copyright (C) 2016 Sébastien Feugère

    This program is free software; you can redistribute it and/or
    modify it under the terms of the the Artistic License (2.0). You
    may obtain a copy of the full license at:

    L<http://www.perlfoundation.org/artistic_license_2_0>

    Any use, modification, and distribution of the Standard or
    Modified Versions is governed by this Artistic License. By using,
    modifying or distributing the Package, you accept this license. Do
    not use, modify, or distribute the Package, if you do not accept
    this license.

    If your Modified Version has been derived from a Modified Version
    made by someone other than you, you are nevertheless required to
    ensure that your Modified Version complies with the requirements
    of this license.

    This license does not grant you the right to use any trademark,
    service mark, tradename, or logo of the Copyright Holder.

    This license includes the non-exclusive, worldwide, free-of-charge
    patent license to make, have made, use, offer to sell, sell,
    import and otherwise transfer the Package with respect to any
    patent claims licensable by the Copyright Holder that are
    necessarily infringed by the Package. If you institute patent
    litigation (including a cross-claim or counterclaim) against any
    party alleging that the Package constitutes direct or contributory
    patent infringement, then this Artistic License to you shall
    terminate on the date that such litigation is filed.

    Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT
    HOLDER AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED
    WARRANTIES.  THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
    FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO
    THE EXTENT PERMITTED BY YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO
    COPYRIGHT HOLDER OR CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT,
    INDIRECT, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING IN ANY WAY
    OUT OF THE USE OF THE PACKAGE, EVEN IF ADVISED OF THE POSSIBILITY
    OF SUCH DAMAGE.

    The comfyss image is from the Wikimedia comfy picture
    https://commons.wikimedia.org/wiki/File:This_dog_is_%22comfy%22-_2013-12-27_10-51.jpg,
    and it is licensed under the Creative Commons Attribution-Share
    Alike 3.0 Unported license.

