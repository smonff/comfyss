#!perl -T
use 5.006;
use strict;
use warnings;
use Test::More;
use CSS::Validation::Comfy;
use feature qw(say);

plan tests => 3;

my $comfyss = CSS::Validation::Comfy->new;
can_ok($comfyss, 'instantiate_watcher');

my $watcher = $comfyss->instantiate_watcher;

isa_ok $watcher, "File::ChangeNotify::Watcher";

# test config
# - path
# - patterns

while (my @events = $watcher->wait_for_events()) {
    for my $event ( @events) {
        # Here, build a file modification mecanism and assert that
        # - the right file has been modified
        # - content of the file has acctually been modified
        say $event->type() . " detected on " . $event->path();

     }

    # break the loop
}


# more tests





