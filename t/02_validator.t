#!perl -T
use 5.006;
use strict;
use warnings;
use Test::More;
use CSS::Validation::Comfy;
use feature qw(say);

plan tests => 3;

my $comfyss = CSS::Validation::Comfy->new;
can_ok($comfyss, 'instantiate_css_validator');

my $validator = $comfyss->instantiate_css_validator;

isa_ok $validator, "WebService::Validator::CSS::W3C";

# skip if not local
like (
    $validator->validator_uri,
    qr/http:\/\/v.css:8080\/css-validator\//,
    "The validator should use the localhost instance");

# more tests





