#!perl -T
use 5.006;
use strict;
use warnings;
use Test::More;

plan tests => 1;

BEGIN {
    use_ok( 'CSS::Validation::Comfy' ) || print "Bail out!\n";
}

diag( "Testing CSS::Validation::Comfy $CSS::Validation::Comfy::VERSION, Perl $], $^X" );
